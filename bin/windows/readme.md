# NTSG Backup Scripts: Windows Build v1.0.0

## Overview

Although written in batch, the Windows versions have been built as executables that include the libraries needed to run it. There is a 32-bit and a 64-bit version of the executable.

You only need the executable, but you can download the `user_config.json.example` as a reference for what the program is expecting.

## Known Issues

- The 32-bit version of the executable may crash if you try to compress a large directory (> 5GB). If you need to compress something larger, then use the 64-bit version.




-------------------------------------------------------------------


__Author:__ Angela Gross

__Application Icon__: Made by [Round Icons](http://www.flaticon.com/authors/roundicons) from www.flaticon.com

__Building Tool__: Built by [Bat to Exe Converter](http://www.f2ko.de/en/b2e.php) from www.f2ko.de

__Created Date:__ 09/30/2016

__Recommended Viewer:__ [Horoopad: Cross Platform Markdown Editor](https://github.com/rhiokim/haroopad) or Bitbucket/Github